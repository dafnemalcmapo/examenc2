function celsiusToFahrenheit(celsius) {
    return (celsius * 9/5) + 32;
}

function fahrenheitToCelsius(fahrenheit) {
    return (fahrenheit - 32) * 5/9;
}

function calcularConversion() {
    const cantidad = parseFloat(document.getElementById("cantidad").value);
    const celsiusRadio = document.getElementById("celsius");
    const resultado = document.getElementById("resultado");

    if (celsiusRadio.checked) {
        const fahrenheit = celsiusToFahrenheit(cantidad);
        resultado.value = fahrenheit.toFixed(2) + "°F";
    } else {
        const celsius = fahrenheitToCelsius(cantidad);
        resultado.value = celsius.toFixed(2) + "°C";
    }
}

function limpiarCampos() {
    document.getElementById("cantidad").value = "";
    document.getElementById("celsius").checked = true;
    document.getElementById("resultado").value = "";
}

document.getElementById("btnCalcular").addEventListener("click", function() {
    calcularConversion();
});

document.getElementById("btnLimpiar").addEventListener("click", function() {
    limpiarCampos();
});
