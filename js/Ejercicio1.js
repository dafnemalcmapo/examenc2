document.addEventListener("DOMContentLoaded", function() {
    function calcularImporte() {
        const tipoViaje = document.getElementById("tipo").value;
        const precioPorBoleto = parseFloat(document.getElementById("precio").value);
        const numeroBoletos = parseInt(document.getElementById("boleto").value);

        let subtotal = precioPorBoleto * numeroBoletos;
        let impuesto = 0;

        if (tipoViaje === "doble") {
            subtotal *= 1.8; // Incrementa el precio en un 80% para viaje doble
        }

        impuesto = subtotal * 0.16; // Calcula el 16% de impuesto

        const total = subtotal + impuesto;

        document.getElementById("subtotal").value = subtotal.toFixed(2);
        document.getElementById("impuesto").value = impuesto.toFixed(2);
        document.getElementById("total").value = total.toFixed(2);
    }

    function limpiarCampos() {
        document.getElementById("boleto").value = "";
        document.getElementById("cliente").value = "";
        document.getElementById("destino").value = "";
        document.getElementById("tipo").value = "sencillo";
        document.getElementById("precio").value = "";
        document.getElementById("subtotal").value = "";
        document.getElementById("impuesto").value = "";
        document.getElementById("total").value = "";
    }

 

    document.getElementById("btnCalcular").addEventListener("click", function() {
        calcularImporte();
    });

    document.getElementById("btnLimpiar").addEventListener("click", function() {
        limpiarCampos();
    });

 
});
